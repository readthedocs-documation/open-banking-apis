# spectral

Spectral, an Open Source JSON/YAML Linter,
creating automated style guides, with baked in support for OpenAPI v2 & v3.
The beginning of an awesome article...

## install spectral

```bash
npm install -g @stoplight/spectral
# vs code
code --install-extension stoplight.spectral
```

## lint files

```bash
spectral lint ./obie/v3.1.8/*.yaml

spectral lint ./obie/v3.1.8/account-info-openapi.yaml
```

## rulesets

| ruleset | .spectral.yml                                                          |
| ------- | ---------------------------------------------------------------------- |
| adidas  | https://github.com/adidas/api-guidelines/blob/master/.spectral.yml     |
| azure   | https://github.com/Azure/azure-api-style-guide/blob/main/spectral.yaml |

## API Design Guidelines

http://apistylebook.com/design/guidelines/
