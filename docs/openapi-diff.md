# openapi-diff

https://github.com/OpenAPITools/openapi-diff

## requirements

- Java 8

## feature

- Supports OpenAPI spec v3.0.
- Depth comparison of parameters, responses, endpoint, http method (GET,POST,PUT,DELETE...)
- Supports swagger api Authorization
- Render difference of property with Expression Language
- HTML, Markdown & JSON render

## docker

https://hub.docker.com/r/openapitools/openapi-diff/
https://github.com/OpenAPITools/openapi-diff/blob/master/Dockerfile

```dockerfile
https://github.com/OpenAPITools/openapi-diff/blob/master/Dockerfile
```

## cli

```
➜  libs git:(master) ✗ java -jar openapi-diff-cli-2.1.0-beta.4-all.jar --help
usage: openapi-diff <old> <new>
    --debug                     Print debugging information
    --error                     Print error information
    --fail-on-changed           Fail if API changed but is backward
                                compatible
    --fail-on-incompatible      Fail only if API changes broke backward
                                compatibility
 -h,--help                      print this message
    --header <property=value>   use given header for authorisation
    --html <file>               export diff as html in given file
    --info                      Print additional information
    --json <file>               export diff as json in given file
 -l,--log <level>               use given level for log (TRACE, DEBUG,
                                INFO, WARN, ERROR, OFF). Default: ERROR
    --markdown <file>           export diff as markdown in given file
    --off                       No information printed
    --query <property=value>    use query param for authorisation
    --state                     Only output diff state: no_changes,
                                incompatible, compatible
    --text <file>               export diff as text in given file
    --trace                     be extra verbose
    --version                   print the version information and exit
    --warn                      Print warning information
```

```bash
java -jar libs/openapi-diff-cli-2.1.0-beta.4-all.jar obie/v3.1.7/account-info-openapi.yaml obie/v3.1.8/account-info-openapi.yaml --markdown docs/account-info/account-info-3.17-3.18.md --log INFO --info
```
