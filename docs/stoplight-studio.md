## stoplight studio

<!-- theme: info -->

API Docs  
![API Docs](../assets/images/stoplight-studio-api-docs.png)

<!-- theme: info -->

Dashboard  
![Dashboard](../assets/images/stoplight-studio-dashboard.png)

<!-- theme: info -->

Mock & Lint
![Mock & Lint](../assets/images/stoplight-studio-mock-lint.png)

```mermaid
journey
    title My working day
    section Go to work
      Make tea: 5: Me
      Go upstairs: 3: Me
      Do work: 1: Me, Cat
    section Go home
      Go downstairs: 5: Me
      Sit down: 5: Me
```

<!--
type: tab
title: My First Tab
-->

The contents of tab 1.

<!--
type: tab
title: My Second Tab
-->

The contents of tab 2.

<!-- type: tab-end -->

```json json_schema
{
    "title": "User",
    "type": "object",
    "properties": {
        "id": {
            "type": "string"
        },
        "name": {
            "type": "string",
            "description": "The user's full name."
        },
        "age": {
            "type": "number",
            "minimum": 0,
            "maximum": 150
        }
    },
    "required": ["id", "name"]
}
```
