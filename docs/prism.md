## prism

Prism is a set of packages for API mocking and contract testing with OpenAPI v2 (formerly known as Swagger) and OpenAPI v3.x.

-   Mock Servers: Life-like mock servers from any API Specification Document.
-   Validation Proxy: Contract Testing for API Consumers and Developers.
-   Comprehensive API Specification Support: OpenAPI 3.0, OpenAPI 2.0 (FKA Swagger) and Postman Collections support.

## install  
```bash
npm install -g @stoplight/prism-cli
```

## usage    
```bash
➜  v3.1.8 git:(master) ✗ prism --help
  prism mock <document>              Start a mock server with the given document file
  prism proxy <document> <upstream>  Start a proxy server with the given document file
```