#### What's Changed

---

##### `GET` /accounts/{AccountId}/statements/{StatementId}/file

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Statements Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `POST` /account-access-consents

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **415 Unsupported Media Type**

> Unsupported Media Type

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **201 Created**

> Account Access Consents Created

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `DELETE` /account-access-consents/{ConsentId}

###### Return Type:

Changed response : **204 No Content**

> Account Access Consents Deleted

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /account-access-consents/{ConsentId}

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Account Access Consents Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /accounts/{AccountId}/balances

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Balances Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /accounts/{AccountId}/direct-debits

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Direct Debits Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /accounts/{AccountId}/offers

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Offers Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /accounts/{AccountId}/parties

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Parties Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /accounts/{AccountId}/party

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Parties Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /accounts/{AccountId}/product

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Products Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /balances

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Balances Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /direct-debits

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Direct Debits Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /offers

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Offers Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /party

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Parties Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /products

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Products Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /accounts

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Accounts Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /accounts/{AccountId}

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Accounts Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /accounts/{AccountId}/scheduled-payments

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Scheduled Payments Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /accounts/{AccountId}/standing-orders

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Standing Orders Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /scheduled-payments

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Scheduled Payments Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /standing-orders

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Standing Orders Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /accounts/{AccountId}/beneficiaries

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Beneficiaries Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /accounts/{AccountId}/statements

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Statements Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /accounts/{AccountId}/statements/{StatementId}

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Statements Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /beneficiaries

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Beneficiaries Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /statements

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Statements Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

##### `GET` /accounts/{AccountId}/statements/{StatementId}/transactions

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Transactions Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

- Changed content type : `application/json; charset=utf-8`

  - Changed property `Data` (object)

    - Changed property `Transaction` (array)

      Changed items (object): > Provides further details on an entry in the report.

      - Changed property `Status` (string)

        > Status of a transaction entry on the books of the account servicer.

        Added enum value:

        - `Rejected`

- Changed content type : `application/json`

  - Changed property `Data` (object)

    - Changed property `Transaction` (array)

      Changed items (object): > Provides further details on an entry in the report.

      - Changed property `Status` (string)

        > Status of a transaction entry on the books of the account servicer.

        Added enum value:

        - `Rejected`

- Changed content type : `application/jose+jwe`

  - Changed property `Data` (object)

    - Changed property `Transaction` (array)

      Changed items (object): > Provides further details on an entry in the report.

      - Changed property `Status` (string)

        > Status of a transaction entry on the books of the account servicer.

        Added enum value:

        - `Rejected`

##### `GET` /accounts/{AccountId}/transactions

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Transactions Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

- Changed content type : `application/json; charset=utf-8`

  - Changed property `Data` (object)

    - Changed property `Transaction` (array)

      Changed items (object): > Provides further details on an entry in the report.

      - Changed property `Status` (string)

        > Status of a transaction entry on the books of the account servicer.

        Added enum value:

        - `Rejected`

- Changed content type : `application/json`

  - Changed property `Data` (object)

    - Changed property `Transaction` (array)

      Changed items (object): > Provides further details on an entry in the report.

      - Changed property `Status` (string)

        > Status of a transaction entry on the books of the account servicer.

        Added enum value:

        - `Rejected`

- Changed content type : `application/jose+jwe`

  - Changed property `Data` (object)

    - Changed property `Transaction` (array)

      Changed items (object): > Provides further details on an entry in the report.

      - Changed property `Status` (string)

        > Status of a transaction entry on the books of the account servicer.

        Added enum value:

        - `Rejected`

##### `GET` /transactions

###### Return Type:

Changed response : **401 Unauthorized**

> Unauthorized

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **404 Not Found**

> Not found

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **405 Method Not Allowed**

> Method Not Allowed

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **406 Not Acceptable**

> Not Acceptable

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **400 Bad Request**

> Bad request

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **403 Forbidden**

> Forbidden

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **500 Internal Server Error**

> Internal Server Error

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

Changed response : **200 OK**

> Transactions Read

Changed header : `x-fapi-interaction-id`

> An RFC4122 UID used as a correlation id.

- Changed content type : `application/json; charset=utf-8`

  - Changed property `Data` (object)

    - Changed property `Transaction` (array)

      Changed items (object): > Provides further details on an entry in the report.

      - Changed property `Status` (string)

        > Status of a transaction entry on the books of the account servicer.

        Added enum value:

        - `Rejected`

- Changed content type : `application/json`

  - Changed property `Data` (object)

    - Changed property `Transaction` (array)

      Changed items (object): > Provides further details on an entry in the report.

      - Changed property `Status` (string)

        > Status of a transaction entry on the books of the account servicer.

        Added enum value:

        - `Rejected`

- Changed content type : `application/jose+jwe`

  - Changed property `Data` (object)

    - Changed property `Transaction` (array)

      Changed items (object): > Provides further details on an entry in the report.

      - Changed property `Status` (string)

        > Status of a transaction entry on the books of the account servicer.

        Added enum value:

        - `Rejected`
