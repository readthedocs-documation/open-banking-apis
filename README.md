# open-banking-apis

## Studio projects structure

```bash
mkdir -p docs reference assets/images
```

`docs` - Where articles (Markdown files) are stored
`reference` - Where API descriptions documents (OpenAPI and JSON Schema) are stored
`assets/images` - Where images are stored

## API Portal

https://microservices-devops.stoplight.io/  
https://microservices-devops.stoplight.io/docs/cHJqOjYwNjYx

## obie

- Open Data API
  https://openbankinguk.github.io/opendata-api-docs-pub/

- Read-Write API
  https://openbankinguk.github.io/read-write-api-site3/v3.1.10/profiles/read-write-data-api-profile.html

- Dynamic Client Registration Specification
  https://openbankinguk.github.io/dcr-docs-pub/

- MI Reporting  
  https://openbankinguk.github.io/mi-docs-pub/v3.1.10-TPP/logical-data-dictionary.html  
  https://openbankinguk.github.io/mi-docs-pub/v3.1.10-aspsp/specification/mi-data-reporting-api-specification.html

## hkma

https://apidocs.hkma.gov.hk/

- [Market Data & Statistics](https://apidocs.hkma.gov.hk/documentation/market-data-and-statistics)
- [Bank & SVF Related Information](https://apidocs.hkma.gov.hk/documentation/bank-svf-info)
- [Financial Market Infrastructure](https://apidocs.hkma.gov.hk/documentation/financial-market-infra)
- [Press Releases](https://apidocs.hkma.gov.hk/documentation/press-releases)
- [inSight Articles](https://apidocs.hkma.gov.hk/documentation/insight-articles)
- [Coin Cart Schedule](https://apidocs.hkma.gov.hk/documentation/coin-cart-schedule)
- [Recruitment](https://apidocs.hkma.gov.hk/documentation/recruitment)

## kubernetes

https://kubernetes.io/releases/
https://github.com/instrumenta/kubernetes-json-schema.git  
Release History

- 1.25
  Latest Release:1.25.3 (released: 2022-10-12)
  End of Life:2023-10-27

https://semver.org/

|                                           |                                                             |
| ----------------------------------------- | ----------------------------------------------------------- |
|                                           | https://www.berlin-group.org/psd2-access-to-bank-accounts   |
| Australia - Consumer Data Right Standards | https://consumerdatastandardsaustralia.github.io/standards/ |
|                                           | https://www.ausbanking.org.au/priorities/open-banking/      |
